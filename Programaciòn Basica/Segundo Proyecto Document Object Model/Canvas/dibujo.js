var texto = document.getElementById("texto_lineas");
var botoncito = document.getElementById("boton");
botoncito.addEventListener("click", dibujoporClick );
var d = document.getElementById("dibujito");
var ancho = d.width;
var lienzo = d.getContext("2d");

function dibujarlinea(color, xinicial, yinicial ,xfinal ,yfinal)
{
lienzo.beginPath();
lienzo.strokeStyle = color;
lienzo.moveTo(xinicial, yinicial);
lienzo.lineTo(xfinal, yfinal);
lienzo.stroke();
lienzo.closePath();
}


function dibujoporClick()
{
    var lineas = parseInt(texto.value);
var l = 0
var xi, yf;
var colorcito = "#FAA";
var espacio = ancho / lineas;
for (l = 0; l < lineas; l++)
{
xi = espacio * l;
yf = espacio * (l + 1);
dibujarlinea(colorcito, 0, xi, yf, 300)
console.log("Linea " + l);
}

dibujarlinea(colorcito, 1, 1, 1, 299)
dibujarlinea(colorcito, 1, 299, 299, 299)
}